package com.rg;

/**
 * Created by rgonciarz on 13/04/17.
 */
public class MessageGroup {

    private final MessageKey messageKey;

    public MessageGroup(MessageKey messageKey) {
        this.messageKey = messageKey;
    }

    public MessageKey getMessageKey() {
        return messageKey;
    }
}
