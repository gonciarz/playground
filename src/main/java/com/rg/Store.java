package com.rg;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created by rgonciarz on 13/04/17.
 */
public class Store {

    private static final RecentGroup recentGroup = new RecentGroup();

    private Map<String, Map<Long, MessageGroup>> eventGroups;

    public MessageGroup createOrGet(MessageKey messageKey) {

        MessageGroup messageGroup = recentGroup.getMessageGroup();
        if (messageGroup.getMessageKey().equals(messageKey)) {
            return messageGroup;
        }

        synchronized (this) {
            String eventId = messageKey.getEventId();
            long publishingTime = messageKey.getPublishingTime();

            Map<Long, MessageGroup> groups = eventGroups.computeIfAbsent(eventId, s -> {
                Map<Long, MessageGroup> map = new TreeMap<>();
                map.put(publishingTime, new MessageGroup(messageKey));
                return map;
            });
            return groups.get(publishingTime);
        }
    }


}
