package com.rg;

/**
 * Created by rgonciarz on 13/04/17.
 */
public class RecentGroup {

    private volatile MessageGroup messageGroup;

    public MessageGroup getMessageGroup() {
        return messageGroup;
    }

    public void setMessageGroup(MessageGroup messageGroup) {
        this.messageGroup = messageGroup;
    }
}
