package com.rg;

/**
 * Created by rgonciarz on 13/04/17.
 */
public class MessageKey {

    private final String eventId;

    private final long publishingTime;

    public MessageKey(String eventId, long publishingTime) {
        this.eventId = eventId;
        this.publishingTime = publishingTime;
    }

    public String getEventId() {
        return eventId;
    }

    public long getPublishingTime() {
        return publishingTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MessageKey that = (MessageKey) o;
        return publishingTime == that.publishingTime && (eventId != null ? eventId.equals(that.eventId) : that.eventId == null);
    }

    @Override
    public int hashCode() {
        int result = eventId != null ? eventId.hashCode() : 0;
        result = 31 * result + (int) (publishingTime ^ (publishingTime >>> 32));
        return result;
    }
}
